{% extends "@admin/plantilla.twig.php" %}

{% block titulo %}Ingresar{% endblock %}

{% block contenido %}
	<form class="form-signin">
		<img class="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
		<h1 class="h2 mb-3 font-weight-normal">Administracion</h1>
		<label for="inputEmail" class="sr-only">Usuario</label>
		<input type="email" id="inputEmail" class="form-control" placeholder="Usuario" required autofocus>
		<label for="inputPassword" class="sr-only">Clave</label>
		<input type="password" id="inputPassword" class="form-control" placeholder="Clave" required>
		<button class="btn btn-primary btn-block" type="submit">Ingresar</button>
		<p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
	</form>
{% endblock %}