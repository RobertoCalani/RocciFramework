<?php
	namespace Sistema;
	/**
	* 
	*/
	class Flash {
		public static function rojo($t = '', $n = 'flash') {
			$_SESSION[$n] = "<div class=\"alert alert-danger\">{$t}</div>";
		}

		public static function verde($t = '', $n = 'flash') {
			$_SESSION[$n] = "<div class=\"alert alert-success\">{$t}</div>";
		}
	}