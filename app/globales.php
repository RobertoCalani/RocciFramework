<?php
	//function ddump($v) { dump($v); exit; }

	function variable($nombre) {

		/*
			Storage
		*/
		$storage = __DIR__ . '/storage/';

		/*
			Ruta BASE
		*/
		$ruta = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
		$ruta .= (sistema('sistema')['base']) ? $_SERVER['HTTP_HOST'] . '/' . trim(sistema('sistema')['base'], '/') : $_SERVER['HTTP_HOST'];



		
		/*
			@return varaible
		*/
		if(!${$nombre}) {
			return '#Variable no encontrada#';
		}
		return ${$nombre};
	}

	function ruta($nombre = '', $parametros = []) {
		$AR = new Sistema\AltoRouter();
		return $AR->generate($nombre, $parametros);
	}

	function sistema($nombre = NULL) {
		global $sistema;
		global $captcha;
		global $correo;
		global $db;
		if(!${$nombre}) {
			return '#Variable no encontrada#';
		}
		return ${$nombre};
	}

	/*
	*
	*	Cargador de campos
	*
	*/
	function campo() {
		$resultado = array();
		foreach ($_POST as $clave => $valor) {
			if(preg_match('/^\w+$/', $clave)) {
				$resultado[$clave] = trim($_POST[$clave]);
			}
		}

		foreach ($_FILES as $clave => $valor) {
			if(preg_match('/^\w+$/', $clave)) {
				if($_FILES[$clave]['tmp_name']) {
					$resultado[$clave] = $_FILES[$clave];
				}
			}
		}
		return $resultado;
	}

	function redireccionar($ruta = '') {
		header("Location: {$ruta}");
		die();
	}